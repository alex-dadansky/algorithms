import numpy as np
from math import sin, cos, pi


def fft(fx):
    """ Реализация дискретного преобразования фуррье `в лоб` """
    N = len(fx)

    result = np.zeros(N, dtype=complex)
    for u in range(N):
        z = np.zeros(2)
        for k in range(N):
            p = 2 * pi * k * u / N
            z[0] += fx[k] * cos(p)
            z[1] += - fx[k] * sin(p)
        result[u] = z[0] + z[1] * 1j
    return result
