import numpy as np
import cooley_tukey
import good_thomas
import simple
import radix
import matplotlib.pyplot as plt

from math import sin, pi
from rsn import Rsn

FD = 8192  # частота дискретизации, отсчетов в секунду
N = 128  # длина входного массива

# чистый сигнал с частотой 440 Гц и длиной N
pure_sig = np.array([6 * abs(int(sin(2. * pi * 440.0 * t / FD)*100)) for t in range(N)])

# шум длиной N
noise = np.random.uniform(0, 100, N)

# суммируем два сигнала и добавим постоянную составляющую 2
signal = pure_sig + noise + 2

# вычисление преобразования Фурье
spectrum = np.fft.fft(signal)
spectrum_simple = simple.fft(signal)
# spectrum_ct = cooley_tukey.fft(signal)
spectrum_radix = radix.fft(signal)
# spectrum_gt = good_thomas.fft(signal)

rsn = cooley_tukey.fft_rsn([Rsn(int(i)) for i in signal])

spectrum_ct = [complex(i) for i in rsn]

# отрисовка зашумленного сигнала синим цветом
plt.plot(np.arange(N) / float(FD), signal)

# отрисовка чистого сигнала красным цветом
plt.plot(np.arange(N) / float(FD), pure_sig, 'r')
plt.xlabel('Время, с')
plt.ylabel('Значение')
plt.title('Чистый сигнал 440 Гц')
plt.grid(True)
plt.show()

xf = np.fft.rfftfreq(N - 1, 1. / FD)

plt.subplot(4, 1, 1)
plt.plot(xf, np.abs(spectrum[:N // 2]))
plt.title("Эталонный спектр")

plt.subplot(4, 1, 2)
plt.plot(xf, np.abs(spectrum_simple[:N // 2]))
plt.title("В лоб спектр")

plt.subplot(4, 1, 3)
plt.plot(xf, np.abs(spectrum_ct[:N // 2]))
plt.title("Кули-тьюки спектр")

plt.subplot(4, 1, 4)
plt.plot(xf, np.abs(spectrum_radix[:N // 2]))
plt.title("БПФ по основанию 2 спектр")

plt.show()
