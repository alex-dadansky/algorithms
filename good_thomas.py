import numpy as np
import matplotlib.pyplot as plt
import cooley_tukey
from time import time

from rsn import RsnComplex, Rsn


def fft_rsn(x):
    """ Алгоритм Кули-Тьюки """
    X = []
    for i in x:
        if isinstance(x, RsnComplex):
            X.append(i)
        else:
            X.append(RsnComplex(i))
    fft_rsn_rec(X)
    return X


def fft_rsn_rec(X):
    N = len(X)

    if N <= 1:
        return

    even = np.array(X[0:N:2])
    odd = np.array(X[1:N:2])

    fft_rsn_rec(even)
    fft_rsn_rec(odd)

    for k in range(0, N // 2):
        r = np.exp(np.complex(0, -2 * np.pi * k / N))
        r *= 10000
        a = Rsn(int(r.real))
        b = Rsn(int(r.imag))
        t = RsnComplex(a, b) * odd[k]
        t = RsnComplex(Rsn(int(t.real) // 10000), Rsn(int(t.imag) // 10000))
        r = even[k] + t
        X[k] = r
        X[N // 2 + k] = even[k] - t

def factor(n):
    """ Разложение числа на простые множители """
    res = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            res.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        res.append(n)
    return res


def get_mpn(n):
    """ Возвращает два числа, произвдение которых равно n """
    f = factor(n)
    l = len(f)
    if l == 1:
        return f[0], 1
    if l == 2:
        return f[0], f[1]
    return f[0], np.prod(f[1:])


def mygcd(a, b):
    u = np.array([1, 0, np.abs(a)])
    v = np.array([0, 1, np.abs(b)])
    while v[2]:
        q = np.floor(u[2] / v[2])
        t = u - v * q
        u = v
        v = t
    c = u[0] * np.sign(a)
    d = u[1] * np.sign(b)
    g = u[2]
    return g, c, d


def low_transform(x, m1, m2):
    if np.gcd(m1, m2) != 1:
        raise Exception()

    res = np.zeros((m1, m2), dtype=complex)

    M = m1 * m2

    g, c, d = mygcd(m1, m2)
    n2 = c
    n1 = (1 - n2 * m2) / m1

    for i in range(0, m1):
        for j in range(0, m2):
            k = np.mod(i * m2 * n2 + np.mod(j * m1 * n1, M), M)
            res[i][j] = x[int(k)]

    return res


def high_transform(x, m1, m2):
    if np.gcd(m1, m2) != 1:
        raise Exception()

    M = m1 * m2
    res = np.zeros(M, dtype=complex)

    for i in range(0, m1):
        for j in range(0, m2):
            k = np.mod(i * m2 + np.mod(j * m1, M), M)
            res[k] = x[i][j]

    return res


def fft(x, N1=7, N2=9):
    """ Алгоритм Гуда-Томаса """
    N = N1 * N2

    if not np.gcd(N1, N1):
        raise Exception()

    if len(x) != N:
        raise Exception()

    transform = low_transform(x, N1, N2)

    temp = np.zeros(N1)
    for i in range(0, N2):
        for j in range(0, N1):
            temp[j] = transform[j][i]
        temp = np.fft.fft(temp)
        for j in range(0, N1):
            transform[j][i] = temp[j]

    temp = np.zeros(N2)
    for i in range(0, N1):
        for j in range(0, N2):
            temp[j] = transform[i][j]
        temp = np.fft.fft(temp)
        for j in range(0, N2):
            transform[i][j] = temp[j]

    return high_transform(transform, N1, N2)


if __name__ == '__main__':
    FD = 8192  # частота дискретизации, отсчетов в секунду

    N1 = 27
    N2 = 34

    N = N1 * N2  # длина входного массива

    # чистый сигнал с частотой 440 Гц и длиной N
    pure_sig = np.array([6 * np.sin(2. * np.pi * 440.0 * t / FD) for t in range(N)])

    # шум длиной N
    noise = np.random.uniform(-50., 50., N)

    # суммируем два сигнала и добавим постоянную составляющую 2
    signal = pure_sig + noise + 2.0

    signal = [int(s) for s in signal]

    # вычисление преобразования Фурье
    lib_time = time()
    spectrum_lib = np.fft.fft(signal)
    lib_time = time() - lib_time
    # вычисление Гуда-ТОмаса в десятичной СС
    gt_time = time()
    spectrum_gt = fft(signal, N1, N2)
    gt_time = time() - gt_time

    # вычисление Гуда-Томаса в СОК
    rsn_time = time()
    spectrum_rsn = fft_rsn([Rsn(i) for i in signal])
    rsn_time = time() - rsn_time
    spectrum_rsn = [complex(i) for i in spectrum_rsn]

    plt.title("Исходный сигнал + шум N = %s " % N)
    # отрисовка зашумленного сигнала синим цветом
    plt.plot(np.arange(N) / float(FD), signal)
    # отрисовка чистого сигнала красным цветом
    plt.plot(np.arange(N) / float(FD), pure_sig, 'r')
    plt.grid(True)
    plt.show()

    xf = np.fft.rfftfreq(N - 2, 1. / FD)

    plt.xlabel('Амплитуда')
    plt.ylabel('Частота')

    plt.subplot(3, 1, 1)
    plt.plot(xf, np.abs(spectrum_lib[:N // 2]))
    plt.title("Спектр сигнала - библиотечная функция (%.3f с)" % lib_time)

    plt.subplot(3, 1, 2)
    plt.plot(xf, np.abs(spectrum_gt[:N // 2]))
    plt.title("Спектр сигнала - алгоритм Гуда-Томаса( %.3f с)" % gt_time)

    plt.subplot(3, 1, 3)
    plt.plot(xf, np.abs(spectrum_rsn[:N // 2]))
    plt.title("Спектр сигнала - алгоритм Гуда-Томаса в СОК( %.3f с)" % rsn_time)

    plt.show()
