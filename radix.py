import numpy as np
import matplotlib.pyplot as plt
import cooley_tukey
from time import time

from rsn import RsnComplex, Rsn


def fft_rsn(x):
    """ Алгоритм Кули-Тьюки """
    X = []
    for i in x:
        if isinstance(x, RsnComplex):
            X.append(i)
        else:
            X.append(RsnComplex(i))
    fft_rsn_rec(X)
    return X


def fft_rsn_rec(X):
    N = len(X)

    if N <= 1:
        return

    even = np.array(X[0:N:2])
    odd = np.array(X[1:N:2])

    fft_rsn_rec(even)
    fft_rsn_rec(odd)

    for k in range(0, N // 2):
        r = np.exp(np.complex(0, -2 * np.pi * k / N))
        r *= 10000
        a = Rsn(int(r.real))
        b = Rsn(int(r.imag))
        t = RsnComplex(a, b) * odd[k]
        t = RsnComplex(Rsn(int(t.real) // 10000), Rsn(int(t.imag) // 10000))
        r = even[k] + t
        X[k] = r
        X[N // 2 + k] = even[k] - t

def fft(x):
    """ Алгоритм БПФ по основанию 2 """
    X = list()
    N = len(x)

    if N <= 1:
        return

    for i in range(0, N):
        X.append(np.complex(x[i] * 1, 0))

    even = np.array(X[0:N:2])
    odd = np.array(X[1:N:2])

    s1 = cooley_tukey.fft(even)
    s2 = cooley_tukey.fft(odd)

    X = s1 + s2

    N = len(X)

    for i in range(0, N // 2):
        x1 = X[i]
        x2 = X[i + N // 2]
        w = np.exp(np.complex(0, -2 * np.pi * i / N))
        X[i] = x1 + x2 * w
        X[i + N // 2] = x1 - x2 * w

    return X


if __name__ == '__main__':

    FD = 8192   # частота дискретизации, отсчетов в секунду
    N = 1024     # длина входного массива

    # чистый сигнал с частотой 440 Гц и длиной N
    pure_sig = np.array([6 * np.sin(2. * np.pi * 440.0 * t / FD) for t in range(N)])

    # шум длиной N
    noise = np.random.uniform(-50., 50., N)

    # суммируем два сигнала и добавим постоянную составляющую 2
    signal = pure_sig + noise + 2.0

    signal = [int(s) for s in signal]

    # вычисление преобразования Фурье
    lib_time = time()
    spectrum_lib = np.fft.fft(signal)
    lib_time = time() - lib_time
    # вычисление Кули-Тьюки в десятичной СС
    rad_time = time()
    spectrum = fft(signal)
    rad_time = time() - rad_time

    # вычисление Кули-Тьюки в СОК
    rsn_time = time()
    spectrum_rsn = fft_rsn([Rsn(i) for i in signal])
    rsn_time = time() - rsn_time
    spectrum_rsn = [complex(i) for i in spectrum_rsn]

    plt.title("Исходный сигнал + шум N = %s " % N)
    # отрисовка зашумленного сигнала синим цветом
    plt.plot(np.arange(N) / float(FD), signal)
    # отрисовка чистого сигнала красным цветом
    plt.plot(np.arange(N) / float(FD), pure_sig, 'r')
    plt.grid(True)
    plt.show()

    xf = np.fft.rfftfreq(N - 2, 1. / FD)

    plt.xlabel('Амплитуда')
    plt.ylabel('Частота')

    plt.subplot(3, 1, 1)
    plt.plot(xf, np.abs(spectrum_lib[:N // 2]))
    plt.title("Спектр сигнала - библиотечная функция (%.3f с)" % lib_time)

    plt.subplot(3, 1, 2)
    plt.plot(xf, np.abs(spectrum[:N // 2]))
    plt.title("Спектр сигнала - БПФ по основанию 2 (%.3f с)" % rad_time)

    plt.subplot(3, 1, 3)
    plt.plot(xf, np.abs(spectrum_rsn[:N // 2]))
    plt.title("Спектр сигнала - БПФ по основанию 2 (%.3f с)" % rsn_time)

    plt.show()