class Rsn(object):
    modules = [3, 5, 7, 11, 13, 17, 19, 23, 29, 31]

    def __init__(self, n):
        self.residues = tuple([n % m for m in self.modules])

    def __str__(self):
        return str(self.residues)

    def __add__(self, other):
        res = Rsn(0)
        residues = []
        for i, m in enumerate(self.modules):
            x = self.residues[i] + other.residues[i]
            residues.append(x % m)
        res.residues = tuple(residues)
        return res

    def __sub__(self, other):
        res = Rsn(0)
        residues = []
        for i in range(0, len(self.modules)):
            x = self.residues[i] - other.residues[i]
            residues.append(x % self.modules[i])
        res.residues = tuple(residues)
        return res

    def __mul__(self, other):
        res = Rsn(0)
        residues = []
        for i, m in enumerate(self.modules):
            x = self.residues[i] * other.residues[i]
            residues.append(x % m)
        res.residues = tuple(residues)
        return res

    def __int__(self):
        max_value = 1
        M = []
        b = []
        for m in self.modules:
            max_value *= m
        for m in self.modules:
            M.append(max_value // m)
        for i in range(0, len(self.modules)):
            for j in range(0, self.modules[i]):
                t = (M[i] * j) % self.modules[i]
                if t == 1:
                    b.append(j)
                    break
        r = 0
        for i in range(0, len(self.modules)):
            r += M[i] * b[i] * self.residues[i]
        r = r % max_value

        if (r > max_value // 2):
            r -= max_value

        return r


class RsnComplex(object):

    def __init__(self, real, imag = Rsn(0)):
        self.real = real
        self.imag = imag

    def __str__(self):
        return "({}, {})".format(int(self.real), int(self.imag))

    def __add__(self, other):
        r = RsnComplex(complex(0))
        r.real = self.real + other.real
        r.imag = self.imag + other.imag
        return r

    def __sub__(self, other):
        r = RsnComplex(complex(0))
        r.real = self.real - other.real
        r.imag = self.imag - other.imag
        return r

    def __mul__(self, other):
        r = RsnComplex(complex(0))
        r.real = self.real * other.real - self.imag * other.imag
        r.imag = self.real * other.imag + self.imag * other.real
        return r

    def __complex__(self):
        return complex(int(self.real), int(self.imag))
