import numpy as np
import matplotlib.pyplot as plt
from time import time

from cooley_tukey import fft_rsn
from rsn import Rsn


def isprime(n):
    """ True если n простое число """
    n = abs(int(n))
    if n < 2:
        return False
    if n == 2:
        return True
    if not n & 1:
        return False
    for x in range(3, int(n ** 0.5) + 1, 2):
        if n % x == 0:
            return False
    return True


def fft(x):
    """ Алгоритм Рейдена """
    N = len(x)

    if not isprime(N):
        raise Exception()
    X = np.array(x)
    data = np.ones((N, N), dtype=complex)

    for i in range(1, N):
        t = i - 1
        k = 0
        for j in range(1, N):
            k = np.mod((k + t), N)
            data[i][j] = np.exp(np.complex(0, -2 * np.pi / N)) ** k

    return data.dot(X)


if __name__ == '__main__':

    FD = 8192   # частота дискретизации, отсчетов в секунду
    N = 919    # длина входного массива

    # чистый сигнал с частотой 440 Гц и длиной N
    pure_sig = np.array([6 * np.sin(2. * np.pi * 440.0 * t / FD) for t in range(N)])

    # шум длиной N
    noise = np.random.uniform(-50., 50., N)

    # суммируем два сигнала и добавим постоянную составляющую 2
    signal = pure_sig + noise + 2.0

    signal = [int(s) for s in signal]

    # вычисление преобразования Фурье
    lib_time = time()
    spectrum_lib = np.fft.fft(signal)
    lib_time = time() - lib_time
    # вычисление Кули-Тьюки в десятичной СС
    r_time = time()
    spectrum = fft(signal)
    r_time = time() - r_time

    # вычисление рейдера в СОК
    rsn_time = time()
    spectrum_rsn = fft_rsn([Rsn(i) for i in signal])
    rsn_time = time() - rsn_time
    spectrum_rsn = [complex(i) for i in spectrum_rsn]

    plt.title("Исходный сигнал + шум N = %s " % N)
    # отрисовка зашумленного сигнала синим цветом
    plt.plot(np.arange(N) / float(FD), signal)
    # отрисовка чистого сигнала красным цветом
    plt.plot(np.arange(N) / float(FD), pure_sig, 'r')
    r_time /= 100
    plt.grid(True)
    plt.show()

    xf = np.fft.rfftfreq(N - 2, 1. / FD)

    plt.xlabel('Амплитуда')
    plt.ylabel('Частота')

    plt.subplot(3, 1, 1)
    plt.plot(xf, np.abs(spectrum_lib[:N // 2]))
    plt.title("Спектр сигнала - библиотечная функция (%.3f с)" % lib_time)

    plt.subplot(3, 1, 2)
    plt.plot(xf, np.abs(spectrum[:N // 2]))
    plt.title("Спектр сигнала - алгоритм Рейдера (%.3f с)" % r_time)

    plt.subplot(3, 1, 3)
    plt.plot(xf, np.abs(spectrum_rsn[:N // 2]))
    plt.title("Спектр сигнала - алгоритм Рейдера в СОК (%.3f с)" % rsn_time)

    plt.show()